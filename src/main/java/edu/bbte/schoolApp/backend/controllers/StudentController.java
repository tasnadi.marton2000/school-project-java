package edu.bbte.schoolApp.backend.controllers;

import edu.bbte.schoolApp.backend.controllers.exception.InternalServerError;
import edu.bbte.schoolApp.backend.controllers.exception.NotFoundException;
import edu.bbte.schoolApp.backend.dto.incoming.GradeIncomingDto;
import edu.bbte.schoolApp.backend.dto.incoming.StudentIncomingDto;
import edu.bbte.schoolApp.backend.dto.outgoing.GradeOutgoingDto;
import edu.bbte.schoolApp.backend.dto.outgoing.StudentOutgoingDto;
import edu.bbte.schoolApp.backend.mapper.GradeMapper;
import edu.bbte.schoolApp.backend.mapper.StudentMapper;
import edu.bbte.schoolApp.backend.model.Grade;
import edu.bbte.schoolApp.backend.model.Student;
import edu.bbte.schoolApp.backend.service.StudentService;
import edu.bbte.schoolApp.backend.service.exception.ServiceException;
import jakarta.validation.Valid;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.*;

import java.util.Collection;
import java.util.Optional;

@RestController
@CrossOrigin
@RequestMapping("api/students")
public class StudentController {
    @Autowired
    StudentService studentService;

    @Autowired
    StudentMapper studentMapper;

    @Autowired
    GradeMapper gradeMapper;

    @PostMapping
    @ResponseStatus(code = HttpStatus.CREATED)
    public StudentOutgoingDto addStudent(@RequestBody @Valid StudentIncomingDto studentIncomingDto) {
        try {
            Student newEntity = studentMapper.incomeToModel(studentIncomingDto);
            Student student = studentService.addStudent(newEntity);
            return studentMapper.modelToOutput(student);
        } catch (ServiceException ex) {
            throw new InternalServerError(ex.getMessage());
        }
    }

    @GetMapping("/{id}")
    public StudentOutgoingDto findStudentById(@PathVariable Long id) {
        try {
            Optional<Student> student = studentService.getStudentById(id);
            if (student.isEmpty()) {
                throw new NotFoundException();
            }
            return studentMapper.modelToOutput(student.get());
        } catch (ServiceException ex) {
            throw new InternalServerError(ex.getMessage());
        }
    }

    @GetMapping
    public Collection<StudentOutgoingDto> findAllStudents() {
        try {
            return studentMapper.modelToListOfOutDto(studentService.getStudents());
        } catch (ServiceException ex) {
            throw new InternalServerError(ex.getMessage());
        }
    }

    @PutMapping("/{id}")
    @ResponseStatus(HttpStatus.NO_CONTENT)
    public void updateStudent(@PathVariable Long id,
                            @RequestBody StudentIncomingDto studentIncomingDto) {
        try {
            Student student = studentMapper.incomeToModel(studentIncomingDto);
            Student newStudent = studentService.updateStudent(id, student);
            if (newStudent == null) {
                throw new NotFoundException();
            }
        } catch (ServiceException ex) {
            throw new InternalServerError(ex.getMessage());
        }
    }

    @DeleteMapping("/{id}")
    @ResponseStatus(HttpStatus.NO_CONTENT)
    public void deleteStudent(@PathVariable("id") Long id) {
        try {
            if (studentService.deleteStudent(id)) {
                throw new NotFoundException();
            }
        } catch (ServiceException ex) {
            throw new InternalServerError(ex.getMessage());
        }
    }

    @GetMapping("/{id}/grades")
    public Collection<Grade> getGradesOfAStudent(@PathVariable Long id) {
        try {
            Collection<Grade> grades = studentService.getGradesOfAStudent(id);
            if (grades == null) {
                throw new NotFoundException();
            }
            return grades;
        } catch (ServiceException ex) {
            throw new InternalServerError(ex.getMessage());
        }
    }

    @PostMapping("/{id}/grades")
    @ResponseStatus(code = HttpStatus.CREATED)
    public GradeOutgoingDto addGradeToAStudent(@PathVariable Long id,
                                               @RequestBody @Valid GradeIncomingDto gradeIncomingDto) {
        try {
            Grade newEntity = gradeMapper.incomeToModel(gradeIncomingDto);
            Grade grade = studentService.addGradeToAStudent(id, newEntity);
            if (grade == null) {
                throw new NotFoundException();
            }
            return gradeMapper.modelToOutput(grade);
        } catch (ServiceException ex) {
            throw new InternalServerError(ex.getMessage());
        }
    }

    @DeleteMapping("/{studentId}/grades/{gradeId}")
    @ResponseStatus(HttpStatus.NO_CONTENT)
    public void deleteGradeOfAStudent(@PathVariable("studentId") Long studentId,
                                      @PathVariable("gradeId") Long gradeId) {
        try {
            if (!studentService.deleteGradeOfAStudent(studentId, gradeId)) {
                throw new NotFoundException();
            }
        } catch (ServiceException ex) {
            throw new InternalServerError(ex.getMessage());
        }
    }
}
