package edu.bbte.schoolApp.backend.controllers;

import edu.bbte.schoolApp.backend.controllers.exception.InternalServerError;
import edu.bbte.schoolApp.backend.controllers.exception.NotFoundException;
import edu.bbte.schoolApp.backend.dto.incoming.ClassIncomingDto;
import edu.bbte.schoolApp.backend.dto.incoming.GradeIncomingDto;
import edu.bbte.schoolApp.backend.dto.incoming.StudentIncomingDto;
import edu.bbte.schoolApp.backend.dto.outgoing.ClassOutgoingDto;
import edu.bbte.schoolApp.backend.dto.outgoing.GradeOutgoingDto;
import edu.bbte.schoolApp.backend.dto.outgoing.StudentOutgoingDto;
import edu.bbte.schoolApp.backend.mapper.ClassMapper;
import edu.bbte.schoolApp.backend.mapper.GradeMapper;
import edu.bbte.schoolApp.backend.mapper.StudentMapper;
import edu.bbte.schoolApp.backend.model.Class;
import edu.bbte.schoolApp.backend.model.Grade;
import edu.bbte.schoolApp.backend.model.Student;
import edu.bbte.schoolApp.backend.service.exception.ServiceException;
import edu.bbte.schoolApp.backend.service.serviceImpl.ClassServiceImpl;
import jakarta.validation.Valid;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.*;

import java.util.Collection;
import java.util.Optional;

@RestController
@CrossOrigin
@RequestMapping("/api/classes")
public class ClassController {

    private final ClassServiceImpl classService;

    private final ClassMapper classMapper;

    private final StudentMapper studentMapper;

    private final GradeMapper gradeMapper;

    @Autowired
    public ClassController(ClassServiceImpl classService, ClassMapper classMapper, StudentMapper studentMapper, GradeMapper gradeMapper) {
        this.classService = classService;
        this.classMapper = classMapper;
        this.studentMapper = studentMapper;
        this.gradeMapper = gradeMapper;
    }

    @PostMapping()
    @ResponseStatus(code = HttpStatus.CREATED)
    @ResponseBody()
    public ClassOutgoingDto addClass(@RequestBody @Valid ClassIncomingDto classIncomingDto) {
        try {
            Class newClass = classMapper.incomeToModel(classIncomingDto);
            Class schoolClass = classService.addClass(newClass);
            return classMapper.modelToOutput(schoolClass);
        } catch (ServiceException ex) {
            throw new InternalServerError(ex.getMessage());
        }
    }

    @GetMapping("/{id}")
    public ClassOutgoingDto findClassById(@PathVariable Long id) {
        try {
            Optional<Class> schoolClass = classService.getClassById(id);
            if (schoolClass.isEmpty()) {
                throw new NotFoundException();
            }
            return classMapper.modelToOutput(schoolClass.get());
        } catch(ServiceException ex) {
            throw new InternalServerError(ex.getMessage());
        }
    }

    @GetMapping
    public Collection<ClassOutgoingDto> findAllClasses() {
        try {
            return classMapper.modelToListOfOutDto(classService.getClasses());
        } catch (ServiceException ex) {
            throw new InternalServerError(ex.getMessage());
        }
    }

    @PutMapping("/{id}")
    @ResponseStatus(HttpStatus.NO_CONTENT)
    public void updateClass(@PathVariable Long id,
                                       @RequestBody ClassIncomingDto classIncomingDto) {
        try {
            Optional<Class> schoolClass = classService.getClassById(id);
            if (schoolClass.isEmpty()) {
                throw new NotFoundException();
            }

            Class newClass = classMapper.incomeToModel(classIncomingDto);
            classMapper.modelToOutput(classService.updateClass(id, newClass));
        } catch(ServiceException ex) {
            throw new InternalServerError(ex.getMessage());
        }
    }

    @DeleteMapping("/{id}")
    @ResponseStatus(HttpStatus.NO_CONTENT)
    public void deleteClass(@PathVariable("id") Long id) {
        try {
            Optional<Class> schoolClass = classService.getClassById(id);
            if (schoolClass.isEmpty()) {
                throw new NotFoundException();
            }

            classService.deleteClass(id);
        } catch (ServiceException ex) {
            throw new InternalServerError(ex.getMessage());
        }
    }

    @GetMapping("/{id}/students")
    public Collection<StudentOutgoingDto> getStudentsOfAClass(@PathVariable Long id) {
        Collection<Student> students = classService.getStudentsOfAClass(id);
        if (students == null) {
            throw new NotFoundException();
        }
        return studentMapper.modelToListOfOutDto(students);
    }

    @PostMapping("/{id}/students")
    @ResponseStatus(code = HttpStatus.CREATED)
    public StudentOutgoingDto addStudentToAClass(@PathVariable Long id,
                                                 @RequestBody @Valid StudentIncomingDto studentIncomingDto) {
        Student newEntity = studentMapper.incomeToModel(studentIncomingDto);
        Student student = classService.addStudentToAClass(id, newEntity);
        if (student == null) {
            throw new NotFoundException();
        }
        return studentMapper.modelToOutput(student);
    }

    @DeleteMapping("/{classId}/students/{studentId}")
    @ResponseStatus(HttpStatus.NO_CONTENT)
    public void deleteStudentOfAClass(@PathVariable("classId") Long classId,
                                       @PathVariable("studentId") Long studentId) {
        if (classService.deleteStudentOfAClass(classId, studentId) == null) {
            throw new NotFoundException();
        }
    }

    @GetMapping("/{classId}/students/{studentId}/grades")
    public Collection<GradeOutgoingDto> getGradesOfAStudentOfAClass(@PathVariable("classId") Long classId,
                                                         @PathVariable("studentId") Long studentId) {
        Collection<Grade> grades = classService.getGradesOfAStudentOfAClass(classId, studentId);
        if (grades == null) {
            throw new NotFoundException();
        }
        return gradeMapper.modelToListOfOutDto(grades);
    }

    @PostMapping("/{classId}/students/{studentId}/grades")
    @ResponseStatus(code = HttpStatus.CREATED)
    public GradeOutgoingDto addGradeToAStudentOfAClass(@PathVariable("classId") Long classId,
                                                       @PathVariable("studentId") Long studentId,
                                                       @RequestBody @Valid GradeIncomingDto gradeIncomingDto) {
        Grade newEntity = gradeMapper.incomeToModel(gradeIncomingDto);
        classService.getClassById(classId).orElseThrow(NotFoundException::new);
        Grade grade = classService.addGradeToAStudentOfAClass(classId, studentId, newEntity);
        if (grade == null) {
            throw new NotFoundException();
        }
        return gradeMapper.modelToOutput(grade);
    }

    @DeleteMapping("/{classId}/students/{studentId}/grades/{gradeId}")
    @ResponseStatus(HttpStatus.NO_CONTENT)
    public void deleteGradeOfAStudentOfAClass(  @PathVariable("classId") Long classId,
                                                @PathVariable("studentId") Long studentId,
                                                @PathVariable("gradeId") Long gradeId ) {
        if (!classService.deleteGradeOfAStudentOfAClass(classId, studentId, gradeId)) {
            throw new NotFoundException();
        }
    }
}
