package edu.bbte.schoolApp.backend.controllers;

import edu.bbte.schoolApp.backend.controllers.exception.InternalServerError;
import edu.bbte.schoolApp.backend.controllers.exception.NotFoundException;
import edu.bbte.schoolApp.backend.dto.incoming.GradeIncomingDto;
import edu.bbte.schoolApp.backend.dto.outgoing.GradeOutgoingDto;
import edu.bbte.schoolApp.backend.mapper.GradeMapper;
import edu.bbte.schoolApp.backend.model.Grade;
import edu.bbte.schoolApp.backend.service.GradeService;
import edu.bbte.schoolApp.backend.service.exception.ServiceException;
import jakarta.validation.Valid;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.*;

import java.util.Collection;
import java.util.Optional;

@RestController
@CrossOrigin
@RequestMapping("api/grades")
public class GradeController {
    private final GradeService gradeService;

    private final GradeMapper gradeMapper;

    @Autowired
    public GradeController(GradeService gradeService, GradeMapper gradeMapper) {
        this.gradeService = gradeService;
        this.gradeMapper = gradeMapper;
    }

    @PostMapping
    @ResponseStatus(code = HttpStatus.CREATED)
    public GradeOutgoingDto addGrade(@RequestBody @Valid GradeIncomingDto gradeIncomingDto) {
        try {
            Grade grade = gradeService.addGrade(gradeMapper.incomeToModel(gradeIncomingDto));
            return gradeMapper.modelToOutput(grade);
        } catch (ServiceException ex) {
            throw new InternalServerError(ex.getMessage());
        }
    }

    @GetMapping("/{id}")
    public GradeOutgoingDto findGradeById(@PathVariable Long id) {
        try {
            Optional<Grade> grade = gradeService.getGradeById(id);
            if (grade.isEmpty()) {
                throw new NotFoundException();
            }
            return gradeMapper.modelToOutput(grade.get());
        } catch (ServiceException ex) {
            throw new InternalServerError(ex.getMessage());
        }
    }

    @GetMapping
    public Collection<GradeOutgoingDto> findAllGrades( @RequestParam(name = "subject", required = false)
                                                           String subject) {
        try {
            if (subject != null) {
                return gradeMapper.modelToListOfOutDto(
                        gradeService.getGradesBySubject(subject));
            }

            return gradeMapper.modelToListOfOutDto(gradeService.getGrades());
        } catch (ServiceException ex) {
            throw new InternalServerError(ex.getMessage());
        }
    }

    @PutMapping("/{id}")
    @ResponseStatus(HttpStatus.NO_CONTENT)
    public void updateGrade(@PathVariable Long id,
                            @RequestBody GradeIncomingDto gradeIncomingDto) {
        try {
            Grade grade = gradeService.updateGrade(id, gradeMapper.incomeToModel(gradeIncomingDto));
            if (grade == null) {
                throw new NotFoundException();
            }
        } catch (ServiceException ex) {
            throw new InternalServerError(ex.getMessage());
        }
    }

    @DeleteMapping("/{id}")
    @ResponseStatus(HttpStatus.NO_CONTENT)
    public void deleteGrade(@PathVariable("id") Long id) {
        try {
            boolean successfull = gradeService.deleteGrade(id);
            if (!successfull) {
                throw new NotFoundException();
            }
        } catch (ServiceException ex) {
            throw new InternalServerError(ex.getMessage());
        }
    }
}
