package edu.bbte.schoolApp.backend.service.serviceImpl;

import edu.bbte.schoolApp.backend.controllers.exception.NotFoundException;
import edu.bbte.schoolApp.backend.mapper.ClassMapper;
import edu.bbte.schoolApp.backend.model.Class;
import edu.bbte.schoolApp.backend.model.Grade;
import edu.bbte.schoolApp.backend.model.Student;
import edu.bbte.schoolApp.backend.repository.ClassJpaRepository;
import edu.bbte.schoolApp.backend.service.ClassService;
import edu.bbte.schoolApp.backend.service.exception.ServiceException;
import org.slf4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.Collection;
import java.util.List;
import java.util.Optional;

@Service
public class ClassServiceImpl implements ClassService {

    @Autowired
    private ClassJpaRepository classJpaRepository;

    @Autowired
    private ClassMapper classMapper;

    @Autowired
    private Logger logger;

    @Override
    public Class addClass(Class schoolClass) throws ServiceException {
        try {
            schoolClass.generateUuid();
            schoolClass = classJpaRepository.saveAndFlush(schoolClass);
            logger.info("Class is added successfully. {}", schoolClass);
            return schoolClass;
        } catch(Exception e) {
            String exceptionMessage = "The adding of a class has failed.";
            logger.error(exceptionMessage);
            throw new ServiceException(exceptionMessage);
        }
    }

    @Override
    public void deleteClass(Long id) throws ServiceException {
        try {
            classJpaRepository.deleteById(id);
            logger.info("Class with id = {} is deleted.", id);
        } catch(Exception e) {
            String exceptionMessage = "The deletion of a class with id " + id + " has failed.";
            logger.error(exceptionMessage);
            throw new ServiceException(exceptionMessage);
        }
    }

    @Override
    public Class updateClass(Long id, Class schoolClass) throws ServiceException {
        try {
            Class oldClass = classJpaRepository.findById(id).orElseThrow(NotFoundException::new);
            oldClass.setNrClass(schoolClass.getNrClass());
            oldClass.setAbbreviation(schoolClass.getAbbreviation());
            oldClass.setProfile(schoolClass.getProfile());
            classJpaRepository.saveAndFlush(oldClass);
            return oldClass;
        } catch(Exception ex) {
            String exceptionMessage = "The update of class with id = " + id + " has failed.";
            logger.error(exceptionMessage);
            throw new ServiceException(exceptionMessage);
        }
    }

    @Override
    public Optional<Class> getClassById(Long id) throws ServiceException {
        try {
            Optional<Class> schoolClass = classJpaRepository.findById(id);
            logger.info("Get class with id = {}", id);
            return schoolClass;
        } catch(Exception ex) {
            String exceptionMessage = "Get class by id = " + id + " has failed.";
            logger.error(exceptionMessage);
            throw new ServiceException(exceptionMessage);
        }
    }

    @Override
    public List<Class> getClasses() throws ServiceException {
        try {
            List<Class> classes = classJpaRepository.findAll();
            logger.info("Get all classes.");
            return classes;
        } catch(Exception ex) {
            String exceptionMessage = "Get all classes has failed.";
            logger.error(exceptionMessage);
            throw new ServiceException(exceptionMessage);
        }
    }

    public Collection<Student> getStudentsOfAClass(Long classId) throws ServiceException {
        try {
            Optional<Class> schoolClass = getClassById(classId);
            logger.info("Get students of the class with id = {}", classId);
            return schoolClass.map(Class::getStudents).orElse(null);
        } catch(Exception ex) {
            String exceptionMessage = "Get students of the class with id = " + classId + " has failed.";
            logger.error(exceptionMessage);
            throw new ServiceException(exceptionMessage);
        }
    }

    public Student addStudentToAClass(Long classId, Student student) throws ServiceException {
        try {
            Optional<Class> resultClass =
                    classJpaRepository.findById(classId);
            if (resultClass.isEmpty()) {
                return null;
            }
            Class schoolClass = resultClass.get();
            Collection<Student> students = schoolClass.getStudents();
            students.add(student);
            schoolClass.setStudents(students);
            classJpaRepository.saveAndFlush(schoolClass);
            logger.info("Student added to the class with id = {} successfully.", classId);
            return (Student) students.toArray()[students.size() - 1];
        } catch(Exception ex) {
            String exceptionMessage = "The addition of a student to a class with the id = " + classId + "has failed.";
            logger.error(exceptionMessage);
            throw new ServiceException(exceptionMessage);
        }
    }

    public Student deleteStudentOfAClass(Long classId, Long studentId) {
        try {
            Optional<Class> resultClass =
                    classJpaRepository.findById(classId);
            if (resultClass.isEmpty()) {
                return null;
            }
            Class schoolClass = resultClass.get();
            Collection<Student> students = schoolClass.getStudents();
            Student stud = null;
            for (Student s : students) {
                if (s.getId().equals(studentId)) {
                    stud = s;
                }
            }
            if (stud == null) {
                return stud;
            }
            students.remove(stud);
            schoolClass.setStudents(students);
            classJpaRepository.saveAndFlush(schoolClass);
            return stud;
        } catch(Exception ex) {
            String exceptionMessage = "The delition of the student = " + studentId
                    + " from the class = " + classId + " has failed.";
            logger.error(exceptionMessage);
            throw new ServiceException(exceptionMessage);
        }
    }

    public Collection<Grade> getGradesOfAStudentOfAClass(Long classId, Long studentId) {
        try {
            Optional<Class> resultClass =
                    classJpaRepository.findById(classId);
            if (resultClass.isEmpty()) {
                return null;
            }
            Class schoolClass = resultClass.get();
            Collection<Student> students = schoolClass.getStudents();
            Student stud = null;
            for (Student s : students) {
                if (s.getId().equals(studentId)) {
                    stud = s;
                }
            }
            if (stud == null) {
                return null;
            }
            logger.info("Get grades of a student of a class.");
            return stud.getGrades();
        } catch(Exception ex) {
            String exceptionMessage = "Get Grades of a Student of a Class has failed.";
            logger.error(exceptionMessage);
            throw new ServiceException(exceptionMessage);
        }
    }

    public Grade addGradeToAStudentOfAClass(Long classId, Long studentId, Grade grade) {
        try {
            Optional<Class> resultClass =
                    classJpaRepository.findById(classId);
            if (resultClass.isEmpty()) {
                return null;
            }
            Class schoolClass = resultClass.get();
            Collection<Student> students = schoolClass.getStudents();
            Student stud = null;
            for (Student s : students) {
                if (s.getId().equals(studentId)) {
                    stud = s;
                }
            }
            if (stud == null) {
                throw new NotFoundException();
            }
            students.remove(stud);
            Collection<Grade> grades = stud.getGrades();
            grades.add(grade);
            stud.setGrades(grades);
            students.add(stud);
            schoolClass.setStudents(students);
            classJpaRepository.saveAndFlush(schoolClass);
            logger.info("A grade is added for a student of a class.");
            return (Grade) students.toArray()[grades.size() - 1];
        } catch(Exception ex) {
            String exceptionMessage = "The addition of a grade to a student of a class has failed";
            logger.error(exceptionMessage);
            throw new ServiceException(exceptionMessage);
        }
    }

    public boolean deleteGradeOfAStudentOfAClass(Long classId, Long studentId, Long gradeId) {
        try {
            Optional<Class> resultClass =
                    classJpaRepository.findById(classId);
            if (resultClass.isEmpty()) {
                return false;
            }
            Class schoolClass = resultClass.get();
            Collection<Student> students = schoolClass.getStudents();
            Student stud = null;
            for (Student s : students) {
                if (s.getId().equals(studentId)) {
                    stud = s;
                }
            }
            if (stud == null) {
                return false;
            }

            Collection<Grade> grades = stud.getGrades();
            Grade gr = null;
            for (Grade g : grades) {
                if (g.getId().equals(gradeId)) {
                    gr = g;
                }
            }
            if (gr == null) {
                return false;
            }
            grades.remove(gr);
            students.remove(stud);
            stud.setGrades(grades);
            students.add(stud);
            schoolClass.setStudents(students);
            classJpaRepository.saveAndFlush(schoolClass);
            logger.info("A grade of a student of a class is deleted successfully.");
            return true;
        } catch(Exception ex) {
            String exceptionMessage = "The delition of a grade of a student of a class is failed.";
            logger.error(exceptionMessage);
            throw new ServiceException(exceptionMessage);
        }
    }
}
