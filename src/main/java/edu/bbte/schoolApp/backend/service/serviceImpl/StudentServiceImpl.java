package edu.bbte.schoolApp.backend.service.serviceImpl;

import edu.bbte.schoolApp.backend.model.Grade;
import edu.bbte.schoolApp.backend.model.Student;
import edu.bbte.schoolApp.backend.repository.StudentJpaRepository;
import edu.bbte.schoolApp.backend.service.StudentService;
import edu.bbte.schoolApp.backend.service.exception.ServiceException;
import org.slf4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.Collection;
import java.util.List;
import java.util.Optional;

@Service
public class StudentServiceImpl implements StudentService {
    @Autowired
    private StudentJpaRepository studentJpaRepository;

    @Autowired
    private Logger logger;

    @Override
    public Student addStudent(Student student) throws ServiceException {
        try {
            Student newEntity = studentJpaRepository.saveAndFlush(student);
            logger.info("Student added successfully.");
            return newEntity;
        } catch (Exception ex) {
            String exceptionMessage = "The addition of a student has failed.";
            logger.error(exceptionMessage);
            throw new ServiceException(exceptionMessage);
        }
    }

    @Override
    public boolean deleteStudent(Long id) throws ServiceException {
        try {
            Optional<Student> student = studentJpaRepository.findById(id);
            if (student.isEmpty()) {
                return false;
            }
            studentJpaRepository.deleteById(id);
            logger.info("Student deleted successfully.");
            return true;
        } catch(Exception ex) {
            String exceptionMessage = "The deletion of a student has failed.";
            logger.error(exceptionMessage);
            throw new ServiceException(exceptionMessage);
        }
    }

    @Override
    public Student updateStudent(Long id, Student student) throws ServiceException {
        try {
            Optional<Student> oldStudent = studentJpaRepository.findById(id);
            if (oldStudent.isEmpty()) {
                return null;
            }
            Student newStudent = oldStudent.get();
            newStudent.setName(student.getName());
            newStudent.setClassId(student.getClassId());
            studentJpaRepository.saveAndFlush(newStudent);
            logger.info("Student updated successfully.");
            return newStudent;
        } catch(Exception ex) {
            String exceptionMessage = "The update of a student has failed.";
            logger.error(exceptionMessage);
            throw new ServiceException(exceptionMessage);
        }
    }

    @Override
    public Optional<Student> getStudentById(Long id) throws ServiceException {
        try {
            Optional<Student> student = studentJpaRepository.findById(id);
            logger.info("Get student by id = {}", id);
            return student;
        } catch(Exception ex) {
            String exceptionMessage = "Get student by id = " + id + " has failed.";
            logger.error(exceptionMessage);
            throw new ServiceException(exceptionMessage);
        }
    }

    @Override
    public List<Student> getStudents() throws ServiceException {
        try {
            List<Student> students = studentJpaRepository.findAll();
            logger.info("Get of all students.");
            return students;
        } catch(Exception ex) {
            String exceptionMessage = "The get of all students has failed.";
            logger.error(exceptionMessage);
            throw new ServiceException(exceptionMessage);
        }
    }

    @Override
    public Collection<Grade> getGradesOfAStudent(Long id) {
        try {
            Optional<Student> student = studentJpaRepository.findById(id);
            logger.info("Get grades of a student by id = {}", id);
            if (student.isEmpty()) {
                return null;
            }
            return student.get().getGrades();
        } catch(Exception ex) {
            String exceptionMessage = "The get grades of a student has failed.";
            logger.error(exceptionMessage);
            throw new ServiceException(exceptionMessage);
        }
    }

    @Override
    public Grade addGradeToAStudent(Long id, Grade grade) {
        try {
            Optional<Student> optionalStudent = studentJpaRepository.findById(id);
            if (optionalStudent.isEmpty()) {
                return null;
            }
            Student student = optionalStudent.get();
            Collection<Grade> grades = student.getGrades();
            grades.add(grade);
            student.setGrades(grades);
            studentJpaRepository.saveAndFlush(student);
            logger.info("Grade of a student is updated successfully.");
            return (Grade) grades.toArray()[grades.size() - 1];
        } catch(Exception ex) {
            String exceptionMessage = "The addition of a grade to a student has failed.";
            logger.error(exceptionMessage);
            throw new ServiceException(exceptionMessage);
        }
    }

    @Override
    public boolean deleteGradeOfAStudent(Long studentId, Long gradeId) {
        try {
            Optional<Student> optionalStudent = studentJpaRepository.findById(studentId);
            if (optionalStudent.isEmpty()) {
                return false;
            }
            Student student = optionalStudent.get();
            Collection<Grade> grades = student.getGrades();
            Grade grade = null;
            for (Grade g : grades) {
                if (g.getId().equals(gradeId)) {
                    grade = g;
                }
            }
            if (grade == null) {
                return false;
            }
            grades.remove(grade);
            student.setGrades(grades);
            studentJpaRepository.saveAndFlush(student);
            logger.info("A grade of a student is deleted successfully.");
            return true;
        } catch(Exception ex) {
            String exceptionMessage = "The deletion of a grade of a student has failed.";
            logger.error(exceptionMessage);
            throw new ServiceException(exceptionMessage);
        }
    }
}
