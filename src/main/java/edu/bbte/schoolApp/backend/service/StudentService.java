package edu.bbte.schoolApp.backend.service;

import edu.bbte.schoolApp.backend.model.Grade;
import edu.bbte.schoolApp.backend.model.Student;
import edu.bbte.schoolApp.backend.service.exception.ServiceException;

import java.util.Collection;
import java.util.List;
import java.util.Optional;

public interface StudentService {
    Student addStudent(Student student) throws ServiceException;

    boolean deleteStudent(Long id) throws ServiceException;

    Student updateStudent(Long id, Student student) throws ServiceException;

    Optional<Student> getStudentById(Long id) throws ServiceException;

    List<Student> getStudents() throws ServiceException;

    Collection<Grade> getGradesOfAStudent(Long id);

    Grade addGradeToAStudent(Long id, Grade grade);

    boolean deleteGradeOfAStudent(Long studentId, Long gradeId);
}
