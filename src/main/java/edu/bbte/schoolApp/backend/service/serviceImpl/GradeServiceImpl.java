package edu.bbte.schoolApp.backend.service.serviceImpl;

import edu.bbte.schoolApp.backend.model.Grade;
import edu.bbte.schoolApp.backend.repository.GradeJpaRepository;
import edu.bbte.schoolApp.backend.repository.exception.RepositoryException;
import edu.bbte.schoolApp.backend.service.GradeService;
import edu.bbte.schoolApp.backend.service.exception.ServiceException;
import org.slf4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.Optional;

@Service
public class GradeServiceImpl implements GradeService {

    @Autowired
    private GradeJpaRepository gradeJpaRepository;

    @Autowired
    private Logger logger;

    @Override
    public Grade addGrade(Grade grade) throws ServiceException {
        try {
            Grade newGrade =  gradeJpaRepository.saveAndFlush(grade);
            logger.info("The grade {} is saved successfully.", newGrade);
            return newGrade;
        } catch (Exception ex) {
            String exceptionMessage = "The save of the grade has failed.";
            logger.error(exceptionMessage);
            throw new ServiceException(exceptionMessage);
        }
    }

    @Override
    public boolean deleteGrade(Long id) throws ServiceException {
        try {
            Optional<Grade> grade = gradeJpaRepository.findById(id);
            if (grade.isEmpty()) {
                return false;
            }
            gradeJpaRepository.deleteById(id);
            logger.info("The grade with id = {} is deleted.", id);
            return true;
        } catch(Exception ex) {
            String exceptionMessage = "The deletion of the grade has failed.";
            logger.error(exceptionMessage);
            throw new ServiceException(exceptionMessage);
        }
    }

    @Override
    public Grade updateGrade(Long id, Grade grade) throws ServiceException {
        try {
            Optional<Grade> oldGrade = gradeJpaRepository.findById(id);
            if (oldGrade.isEmpty()) {
                return null;
            }
            Grade newGrade = oldGrade.get();
            newGrade.setGrade(grade.getGrade());
            newGrade.setType(grade.getType());
            newGrade.setDate(grade.getDate());
            newGrade.setSubject(grade.getSubject());
            newGrade.setNotes(grade.getNotes());
            gradeJpaRepository.saveAndFlush(newGrade);
            logger.info("The grade with id = {} is updated successfully.", id);
            return newGrade;
        } catch(Exception ex) {
            String exceptionMessage = "The update of the grade has failed.";
            logger.error(exceptionMessage);
            throw new ServiceException(exceptionMessage);
        }
    }

    @Override
    public Optional<Grade> getGradeById(Long id) throws ServiceException {
        try {
            Optional<Grade> grade = gradeJpaRepository.findById(id);
            logger.info("Get grade by id.");
            return grade;
        } catch(Exception ex) {
            String exceptionMessage = "The get grade by id has failed.";
            logger.error(exceptionMessage);
            throw new ServiceException(exceptionMessage);
        }
    }

    @Override
    public List<Grade> getGrades() throws ServiceException {
        try {
            List<Grade> grades = gradeJpaRepository.findAll();
            logger.info("Get all grades.");
            return grades;
        } catch(Exception ex) {
            String exceptionMessage = "Get all grades has failed.";
            logger.error(exceptionMessage);
            throw new ServiceException(exceptionMessage);
        }
    }

    public List<Grade> getGradesBySubject(String subject) throws ServiceException {
        try {
            return gradeJpaRepository.findBySubject(subject);
        } catch(RepositoryException ex) {
            String exceptionMessage = "Get grades by subject " + subject + " has failed.";
            logger.error(exceptionMessage);
            throw new ServiceException(exceptionMessage);
        }
    }
}
