package edu.bbte.schoolApp.backend.service;

import edu.bbte.schoolApp.backend.model.Class;
import edu.bbte.schoolApp.backend.model.Grade;
import edu.bbte.schoolApp.backend.model.Student;
import edu.bbte.schoolApp.backend.service.exception.ServiceException;

import java.util.Collection;
import java.util.List;
import java.util.Optional;

public interface ClassService {
    Class addClass(Class schoolClass) throws ServiceException;
    void deleteClass(Long id) throws ServiceException;
    Class updateClass(Long id, Class schoolClass) throws ServiceException;
    Optional<Class> getClassById(Long id) throws ServiceException;
    List<Class> getClasses() throws ServiceException;
    Collection<Student> getStudentsOfAClass(Long classId);
    Student addStudentToAClass(Long classId, Student student);
    Student deleteStudentOfAClass(Long classId, Long studentId);
    Collection<Grade> getGradesOfAStudentOfAClass(Long classId, Long studentId);
    Grade addGradeToAStudentOfAClass(Long classId, Long studentId, Grade grade);
    boolean deleteGradeOfAStudentOfAClass(Long classId, Long studentId, Long gradeId);

}
