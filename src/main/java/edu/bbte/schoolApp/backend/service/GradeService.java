package edu.bbte.schoolApp.backend.service;

import edu.bbte.schoolApp.backend.model.Class;
import edu.bbte.schoolApp.backend.model.Grade;
import edu.bbte.schoolApp.backend.service.exception.ServiceException;

import java.util.List;
import java.util.Optional;

public interface GradeService {
    Grade addGrade(Grade grade) throws ServiceException;

    boolean deleteGrade(Long id) throws ServiceException;

    Grade updateGrade(Long id, Grade grade) throws ServiceException;

    Optional<Grade> getGradeById(Long id) throws ServiceException;

    List<Grade> getGrades() throws ServiceException;

    List<Grade> getGradesBySubject(String subject);
}
