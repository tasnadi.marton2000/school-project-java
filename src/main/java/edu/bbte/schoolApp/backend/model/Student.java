package edu.bbte.schoolApp.backend.model;

import edu.bbte.schoolApp.backend.annotation.IgnoreColumn;
import jakarta.persistence.*;
import lombok.*;

import java.util.Collection;

@Data
@EqualsAndHashCode(callSuper = true)
@ToString(callSuper = true)
@AllArgsConstructor
@NoArgsConstructor
@Entity
@Table(name = "students")
public class Student extends BaseEntity{
    private String name;

    @IgnoreColumn
    @OneToMany(cascade = CascadeType.ALL,orphanRemoval = true, fetch = FetchType.LAZY)
    @JoinColumn(name = "studentId")
    private Collection<Grade> grades;

    private Long ClassId;
}
