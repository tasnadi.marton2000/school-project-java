package edu.bbte.schoolApp.backend.model;

import jakarta.persistence.Entity;
import jakarta.persistence.Table;
import lombok.*;

import java.util.Date;

@Data
@EqualsAndHashCode(callSuper = true)
@ToString(callSuper = true)
@AllArgsConstructor
@NoArgsConstructor
@Entity
@Table(name = "grades")
public class Grade extends BaseEntity{
    private int grade;

    private String notes;

    private String subject;

    private Long studentId;

    private Date date;

    private String type;
}
