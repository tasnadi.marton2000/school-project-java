package edu.bbte.schoolApp.backend.model;

import edu.bbte.schoolApp.backend.annotation.IgnoreColumn;
import jakarta.persistence.*;
import lombok.*;

import java.util.Collection;

@Data
@EqualsAndHashCode(callSuper = true)
@ToString(callSuper = true)
@AllArgsConstructor
@NoArgsConstructor
@Entity
@Table(name = "classes")
public class Class extends BaseEntity{
    private int nrClass;

    private String abbreviation;

    private String profile;

    @IgnoreColumn
    @OneToMany(cascade = CascadeType.ALL,orphanRemoval = true, fetch = FetchType.LAZY)
    @JoinColumn(name = "classId")
    private Collection<Student> students;
}
