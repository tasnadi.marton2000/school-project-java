package edu.bbte.schoolApp.backend.repository;

import edu.bbte.schoolApp.backend.model.Student;
import org.springframework.data.jpa.repository.JpaRepository;

public interface StudentJpaRepository extends JpaRepository<Student, Long> {
}
