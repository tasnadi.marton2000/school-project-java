package edu.bbte.schoolApp.backend.repository;

import edu.bbte.schoolApp.backend.model.Class;
import org.springframework.data.jpa.repository.JpaRepository;

public interface ClassJpaRepository extends JpaRepository<Class, Long> {
}
