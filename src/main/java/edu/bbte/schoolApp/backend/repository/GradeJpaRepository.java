package edu.bbte.schoolApp.backend.repository;

import edu.bbte.schoolApp.backend.model.Grade;
import edu.bbte.schoolApp.backend.repository.exception.RepositoryException;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;

import java.util.List;

public interface GradeJpaRepository extends JpaRepository<Grade, Long> {

    @Query("SELECT g FROM Grade g WHERE g.subject = ?1")
    List<Grade> findBySubject(String username) throws RepositoryException;
}
