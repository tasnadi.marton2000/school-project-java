package edu.bbte.schoolApp.backend.dto.incoming;

import jakarta.validation.constraints.NotEmpty;
import jakarta.validation.constraints.NotNull;
import jakarta.validation.constraints.PositiveOrZero;
import jakarta.validation.constraints.Size;
import lombok.Data;

import java.io.Serializable;

@Data
public class ClassIncomingDto implements Serializable {
    @NotNull
    @PositiveOrZero
    private Integer nrClass;

    @NotNull
    @NotEmpty
    @Size(max = 10)
    private String abbreviation;

    @NotNull
    @NotEmpty
    @Size(max = 100)
    private String profile;
}
