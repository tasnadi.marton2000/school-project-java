package edu.bbte.schoolApp.backend.dto.outgoing;

import lombok.Data;

import java.io.Serializable;
import java.util.Date;

@Data
public class GradeOutgoingDto implements Serializable {
    private Long id;

    private int grade;

    private String notes;

    private String subject;

    private Long studentId;

    private Date date;

    private String type;
}
