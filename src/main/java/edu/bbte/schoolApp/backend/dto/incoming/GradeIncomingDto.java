package edu.bbte.schoolApp.backend.dto.incoming;

import jakarta.validation.constraints.NotEmpty;
import jakarta.validation.constraints.NotNull;
import jakarta.validation.constraints.PositiveOrZero;
import jakarta.validation.constraints.Size;
import lombok.Data;

import java.io.Serializable;
import java.util.Date;

@Data
public class GradeIncomingDto implements Serializable {
    @NotNull
    @PositiveOrZero
    private Integer grade;

    @NotNull
    @NotEmpty
    @Size(max = 400)
    private String notes;

    @NotNull
    @NotEmpty
    @Size(max = 100)
    private String subject;

    @NotNull
    private Long studentId;

    @NotNull
    private Date date;

    @NotNull
    @NotEmpty
    private String type;
}
