package edu.bbte.schoolApp.backend.dto.outgoing;

import edu.bbte.schoolApp.backend.model.Student;
import lombok.Data;

import java.io.Serializable;
import java.util.Collection;

@Data
public class ClassOutgoingDto implements Serializable {
    private Long id;

    private int nrClass;

    private int classId;

    private String abbreviation;

    private String profile;
}
