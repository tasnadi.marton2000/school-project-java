package edu.bbte.schoolApp.backend.dto.outgoing;

import edu.bbte.schoolApp.backend.model.Grade;
import lombok.Data;

import java.io.Serializable;
import java.util.Collection;

@Data
public class StudentOutgoingDto implements Serializable {
    private Long id;

    private String name;

    private Collection<Grade> grades;

    private Long ClassId;
}
