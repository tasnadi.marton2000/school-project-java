package edu.bbte.schoolApp.backend.dto.incoming;

import jakarta.validation.constraints.NotEmpty;
import jakarta.validation.constraints.NotNull;
import jakarta.validation.constraints.Size;
import lombok.Data;

import java.io.Serializable;

@Data
public class StudentIncomingDto implements Serializable {
    @NotNull
    @NotEmpty
    @Size(max = 100)
    private String name;

    @NotNull
    private Long ClassId;
}
