package edu.bbte.schoolApp.backend.mapper;

import edu.bbte.schoolApp.backend.dto.incoming.ClassIncomingDto;
import edu.bbte.schoolApp.backend.dto.incoming.GradeIncomingDto;
import edu.bbte.schoolApp.backend.dto.outgoing.ClassOutgoingDto;
import edu.bbte.schoolApp.backend.dto.outgoing.GradeOutgoingDto;
import edu.bbte.schoolApp.backend.model.Class;
import edu.bbte.schoolApp.backend.model.Grade;
import org.mapstruct.IterableMapping;
import org.mapstruct.Mapper;
import org.mapstruct.ReportingPolicy;
import org.springframework.stereotype.Repository;

import java.util.Collection;

@Mapper(componentModel="spring", unmappedTargetPolicy = ReportingPolicy.IGNORE)
public interface GradeMapper {
    GradeOutgoingDto modelToOutput(Grade grade);

    Grade incomeToModel(GradeIncomingDto gardeIncomingDto);

    @IterableMapping(elementTargetType = GradeOutgoingDto.class)
    Collection<GradeOutgoingDto> modelToListOfOutDto(
            Collection<Grade> grades);
}
