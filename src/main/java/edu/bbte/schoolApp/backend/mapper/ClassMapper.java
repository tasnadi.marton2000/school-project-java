package edu.bbte.schoolApp.backend.mapper;

import edu.bbte.schoolApp.backend.dto.incoming.ClassIncomingDto;
import edu.bbte.schoolApp.backend.dto.outgoing.ClassOutgoingDto;
import edu.bbte.schoolApp.backend.model.Class;
import org.mapstruct.IterableMapping;
import org.mapstruct.Mapper;
import org.mapstruct.ReportingPolicy;

import java.util.Collection;

@Mapper(componentModel="spring", unmappedTargetPolicy = ReportingPolicy.IGNORE)
public interface ClassMapper {
    ClassOutgoingDto modelToOutput(Class schoolClass);

    Class incomeToModel(ClassIncomingDto classIncomingDto);

    @IterableMapping(elementTargetType = ClassOutgoingDto.class)
    Collection<ClassOutgoingDto> modelToListOfOutDto(
            Collection<Class> classes);
}
