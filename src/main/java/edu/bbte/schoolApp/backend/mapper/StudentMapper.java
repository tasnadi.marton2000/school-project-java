package edu.bbte.schoolApp.backend.mapper;

import edu.bbte.schoolApp.backend.dto.incoming.GradeIncomingDto;
import edu.bbte.schoolApp.backend.dto.incoming.StudentIncomingDto;
import edu.bbte.schoolApp.backend.dto.outgoing.GradeOutgoingDto;
import edu.bbte.schoolApp.backend.dto.outgoing.StudentOutgoingDto;
import edu.bbte.schoolApp.backend.model.Grade;
import edu.bbte.schoolApp.backend.model.Student;
import org.mapstruct.IterableMapping;
import org.mapstruct.Mapper;
import org.mapstruct.ReportingPolicy;
import org.springframework.stereotype.Repository;

import java.util.Collection;

@Mapper(componentModel="spring", unmappedTargetPolicy = ReportingPolicy.IGNORE)
public interface StudentMapper {
    StudentOutgoingDto modelToOutput(Student student);

    Student incomeToModel(StudentIncomingDto studentIncomingDto);

    @IterableMapping(elementTargetType = StudentOutgoingDto.class)
    Collection<StudentOutgoingDto> modelToListOfOutDto(
            Collection<Student> grades);
}
